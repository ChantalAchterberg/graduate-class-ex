## Set up
* Download [Git](https://git-scm.com/downloads)
* [Clone](https://help.github.com/en/articles/cloning-a-repository) this repository (IWNET)
* Start programming!

## Exercise 1
Create a simple self-checkout system. Prompt for the prices
and quantities of three items. Calculate the subtotal of the
items. Then calculate the tax using a tax rate of 5.5%. Print
out the line items with the quantity and total, and then print
out the subtotal, tax amount, and total.
### Example output

```
Enter the price of item 1: 25
Enter the quantity of item 1: 2
Enter the price of item 2: 10
Enter the quantity of item 2: 1
Enter the price of item 3: 4
Enter the quantity of item 3: 1
Subtotal: $64.00
Tax: $3.52
Total: $67.52
```

### Constraints
* Keep the input, processing, and output parts of your
program separate. Collect the input, then do the math
operations and string building, and then print out the
output.
* Be sure you explicitly convert input to numerical data
before doing any calculations.

### Extra Challenges
* Revise the program to ensure that prices and quantities
are entered as numeric values. Don’t allow the user to
proceed if the value entered is not numeric.
* Alter the program so that an indeterminate number of
items can be entered. The tax and total are computed
when there are no more items to be entered.

## Exercise 2
Pulling data from a file into a complex data structure makes
parsing much simpler. Many programming languages support
the JSON format, a popular way of representing data.
Create a program that takes a product name as input and
retrieves the current price and quantity for that product. The
product data is in a data file in the JSON format and looks
like this:
```
{
  "products" : [
    {"name": "Widget", "price": 25.00, "quantity": 5 },
    {"name": "Thing", "price": 15.00, "quantity": 5 },
    {"name": "Doodad", "price": 5.00, "quantity": 10 }
  ]
}
```
Print out the product name, price, and quantity if the product
is found. If no product matches the search, state that no
product was found and start over.

### Example output
```
What is the product name? iPad
Sorry, that product was not found in our inventory.
What is the product name? Widget
Name: Widget
Price: $25.00
Quantity on hand: 5
```
### Constraints
* The file is in the JSON format. Use a JSON parser to pull
the values out of the file.
* If no record is found, prompt again.

### Extra Challenges
* Ensure that the product search is case insensitive.
* When a product is not found, ask if the product should
be added. If yes, ask for the price and the quantity, and
save it in the JSON file. Ensure the newly added product
is immediately available for searching without restarting
the program.
