﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _44___Product_Search
{
    class Program
    {
        static void Main(string[] args)
        {
            string productList = System.IO.File.ReadAllText(@"C:\Users\achterbergc\source\repos\Exercises for Programmers\44 - Product Search\ProductList.json");
            List<Product> myList = JsonConvert.DeserializeObject<List<Product>>(productList);

            bool searchProduct = true;
            while (searchProduct)

            {
                double price;
                int quantity;
                string answer;

                Console.WriteLine("What is the product name?");
                string name = Console.ReadLine();

                if (myList.Exists(p => p._name.Equals(name, StringComparison.OrdinalIgnoreCase)))
                {
                    ShowProductDetails(myList, name);
                }

                else
                {
                    Console.WriteLine("Sorry, that product was not found in our inventory. Do you want to add this product to the product list? (Y/N)");
                    answer = Console.ReadLine();
                    string AddProduct = CheckForYorN(answer);

                    if (AddProduct == "Y")
                    {
                        price = AskPriceAndCheckInput();
                        quantity = AskQuantityAndCheckInput();

                        Product product = new Product(name, price, quantity);
                        myList.Add(product);

                        string json = JsonConvert.SerializeObject(myList.ToArray());

                        //write string to file
                        System.IO.File.WriteAllText(@"C:\Users\achterbergc\source\repos\Exercises for Programmers\44 - Product Search\ProductList.json", json);

                        Console.WriteLine(name + " is added to the inventory.");

                    }
                }

                Console.WriteLine("Do you want to search for another product? (Y/N)");
                    answer = Console.ReadLine();
                    searchProduct = (CheckForYorN(answer)=="Y");

            }

           
        }

        private static int AskQuantityAndCheckInput()
        {
            int quantity;
            Console.WriteLine("What is the quantity of the product");
            while (!int.TryParse(Console.ReadLine(), out quantity))
            {
                Console.WriteLine("Please enter a valid quantity.");

            }

            return quantity;
        }

        private static double AskPriceAndCheckInput()
        {
            double price;
            Console.WriteLine("What is the price of the product?");
            string input = Console.ReadLine().Replace(".", ",");

            while (!double.TryParse(input, out price))
            {
                Console.WriteLine("Please enter a valid number.");
                input = Console.ReadLine();
            }

            return price;
        }

        private static string CheckForYorN(string answer)
        {
            while (answer != "Y" & answer != "N")
            {
                Console.WriteLine("Please type 'Y' for Yes or 'N' for No.");
                answer = Console.ReadLine();
            }

            return answer;
        }

        private static void ShowProductDetails(List<Product> myList, string name)
        {
            var askedProduct = myList.Where(p => p._name.ToUpper() == name.ToUpper());
            foreach (var product in askedProduct)
            {
                Console.WriteLine("Name:" + product._name);
                Console.WriteLine("Price:" + product._price);
                Console.WriteLine("Quantity on hand:" + product._quantity);
            }
        }
    }

    internal class Product
    {

        public string _name { get; set;  }
        public double _price { get; set; }
        public int _quantity { get; set; }

        public Product(string name, double price, int quantity)
        {
            _name = name;
            _price = price;
            _quantity = quantity; 
        }

    }

}
