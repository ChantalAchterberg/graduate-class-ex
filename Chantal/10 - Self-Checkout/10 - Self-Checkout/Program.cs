﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10___Self_Checkout
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Default;

            List<Item> myList = new List<Item>();
            bool anotherItem = true;
            int i = 1;
            while (anotherItem)

            {
                double price;
                int quantity;
                askPriceAndQuantity(i, out price, out quantity);

                Item item = new Item(price, quantity);
                myList.Add(item);

                Console.WriteLine("Do you want to add another item? Type Y or N.");

                string AddItem = Console.ReadLine();

                while (AddItem != "Y" & AddItem != "N")
                {
                    Console.WriteLine("Please type 'Y' for Yes or 'N' for No.");
                    AddItem = Console.ReadLine();

                }

                anotherItem = (AddItem == "Y");
                i += 1;


            };
            double subtotal;
            double tax;

            calculateSubtotalAndTax(myList, out subtotal, out tax);

            printTotalsAndTax(subtotal, tax);

        }

        private static void askPriceAndQuantity(int i, out double price, out int quantity)
        {
            Console.WriteLine("Enter the price of item " + i + " : ");
            string input = Console.ReadLine().Replace(".", ",");

            while (!double.TryParse(input, out price))
            {
                Console.WriteLine("Please enter a valid number");
                input = Console.ReadLine(); 
            }

            Console.WriteLine("Enter the quantity of item " + i + " : ");
            while (!int.TryParse(Console.ReadLine(), out quantity))
            {
                Console.WriteLine("Please enter a valid quantity");
            }
        }

        private static void calculateSubtotalAndTax(List<Item> myList, out double subtotal, out double tax)
        {
            subtotal = myList.Sum(p => p._price * p._quantity);
            tax = subtotal * 0.055;
        }

        private static void printTotalsAndTax(double subtotal, double tax)
        {
            Console.WriteLine(string.Format("Subtotal = {0:C}", subtotal));
            Console.WriteLine(string.Format("Tax = {0:C}", tax));
            Console.WriteLine(string.Format("Total = {0:C}", (subtotal + tax)));
            Console.ReadLine();
        }
    }

    }

    class Item
    {
    

    public double _price { get; set; }
    public int _quantity { get; set; }

    public Item(double price, int quantity)
    {
       _price = price; 
       _quantity = quantity;
        
    }
    }

